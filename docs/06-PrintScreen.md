Print Screen
============
Capturing the screen or the window of a certain application is a pretty low level operation. Nevertheless, it is many times useful, for instance, to report software bugs, to illustrate a presentation or simply to report information to a colleague. This function thus tends to have more relevance than one would expect.

The Print Screen key is an artefact of the command line age, that directed PC operating systems to flush all characters visible in the screen to the standard printer port. As it acquired a new functionality in modern graphical user interfaces  this key prevails in most keyboards, even compact ones. Any other DE captures this key input and creates directly the expect screen shot. This is not the case with i3, being up to the user to set up such functionality.

Making Print Screen function as expected is not difficult, it simply requires a programme to handle the request and its wiring through the inescapable `config` file. In this example the screen-saver from the Gnome DE is used for this purpose. The first task to the check whether it is installed:

```
sudo apt install gnome-screenshot
```

Then the wiring in the `config` file:

```
# Interactive screenshot with Print Screen key
bindsym Print exec gnome-screenshot -i
```

The `gnome-screenshot` programme has various handy operation modes, than can too be wired in the `config` file. For example, it can capture exclusively an area selected with the mouse. This can be set up with a key combination like: 

```
# Crop-area screenshot with Mod + Print Screen combination
bindsym $mod+Print exec gnome-screenshot -a
```
