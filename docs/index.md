# i3 for Unity users

This manual details how to set up the [i3](https://i3wm.org/) desktop environment on Ubuntu. Concieved for users of the now defunct [Unity](https://en.wikipedia.org/wiki/Unity_%28user_interface%29) desktop environment.

Table of Contents
-----------------

- [Motivation](01-Motivation)

- [Internet](02-Internet)

- [StartUp](03-StartUp)

- [Sound and media](04-SoundMedia)

- [Screen Management](05-ScreenManagement)

- [Print screen](06-PrintScreen)

- [Suspend, lock and shutdown](07-SuspendLockShutdown)

- [More](08-More)


Copyright
--------------------------------------------------------------------------------

Copyright 2019 Luís Moreira de Sousa. All rights reserved.
Any use of these documents constitutes full acceptance of all terms of the
documents licence.

Licence
-------

This set of scripts is released under the European Union Public Licence v 1.2. Please consult the [LICENCE](https://codeberg.org/ldesousa/i3-for-unity/src/branch/master/LICENCE) file for details.

