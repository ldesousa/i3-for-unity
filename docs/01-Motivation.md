Motivation
==========

The good days of Unity
----------------------

The Unity desktop environment (DE) was introduced by Canonical to the main Ubuntu distribution in 2011. It was a radical answer to the radical challenge posed by version 3 of the popular Gnome DE. Unity shipped initially with a number of odds and ends that made its use somewhat awkward. However, when Ubuntu 12.04 was released Unity had fully matured to what remains today the most agile DE ever developed for Linux. It is to this day the only DE that both me and my mother were happy with. It served simultaneously my mother's needs of simplicity and mouse-centric interaction and my requirements of fast, keyboard-lead window and workspace management.

But it all went up in smoke when Canonical decided to halt development in 2017, to re-centre its business from the end user to service providers and the cloud. A number of initiatives sprouted, to either continue or re-make Unity, but none have survived.

I kept using Unity for as long as I could, but with Ubuntu 18.04 a series of quirks appeared. Moreover, Unity keeps relying on Gnome 3 applications, most of which have been so drained of functionality than are no longer useful.  Unity is showing its age, and is unlikely to survive another Ubuntu long-term service release. Therefore, at the onset of 2019 I started seriously searching for a replacement. 

The i3 epiphany
---------------

Ubuntu itself accommodates different desktop environments that help dealing with original issues with Gnome 3. KDE, xfce, Mate, each have their strengths and weaknesses. Some have awesome applications like KDE, others retain an old but familiar interface paradigm like Mate, but none allows the kind of keyboard-based agility that Unity used to provide.

Seeking out of the box I eventually came across i3. I had seen it in action before, but somehow it never made into my "to try" list. And being packaged for Ubuntu, it was just an install command away. After learning the basics, it became clear i3 was what I was looking for. Moving windows across workspaces with a single key combination, accessing workspaces with a single key combination, tilling windows with single key combination. i3 does all that Unity did, and then more.

That was the happy beginning, but soon I realised I was up for a major struggle. If i3 excels in window/workspace management, it provides hardly anything regarding system management. Soon enough I was hitting mail lists and StackExchange with questions as simple as setting the volume up or down. And after a few days on this another thing became clear, the i3 crowd is not exactly interested in such questions. i3 is strictly a window manager and all the rest is up to the user to figure out.

Possibly, this would have been the end of the road for most people. But my stubbornness kept me engaged: i3 would not defeat me. After a couple of excruciating weeks, I finally arrived at a stage were I become comfortable enough to start using it even in my work environment. There are still many loose ends to tie in, but for the large part I have a usable i3 setup.

This manual intends to help other users settling in with i3. It is primarily directed at users familiar with Ubuntu and Unity, but it should be useful to everyone else. It goes through basic interaction with peripherals, start-up applications and customisation. And hopefully will invite others to contribute.

Starting with i3
----------------

This document does not intend to explain what is i3 or introduce its usage, that topic is already well covered out there in the web. However, the following contents are recommended:

- [i3 User's Guide](https://i3wm.org/docs/userguide.html)
- [Getting started with the i3 tiling window manager](https://fedoramagazine.org/getting-started-i3-window-manager/)
- [i3 Reference Card](https://i3wm.org/docs/refcard.html)
- [i3wm: Jump Start](https://www.youtube.com/watch?v=j1I63wGcvU4) (video)
- [i3wm: Configuration](https://www.youtube.com/watch?v=8-S0cWnLBKg) (video)
- [i3wm: How to "Rice" Your Desktop](https://www.youtube.com/watch?v=ARKIwOlazKI&t=679s) (video)



