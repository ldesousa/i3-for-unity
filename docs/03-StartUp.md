Start up applications
=====================

The Start up applications menu in Unity provides a fairly simple to use graphical interface where the user may set the applications that must run when the session begins. The concept is similar in i3, the difference being the absence of a graphical interface. In i3 start up applications are set in the `config` file itself, with the `exec` directive preceding the executable of the desirable application. For instance, to launch `evolution` at start up, the following must be added to `config`:

```
exec --no-startup-id evolution 
```

It is good practice to keep start up applications tidy in their own section within the `config` file. The code segment bellow exemplifies a start up applications section, launching the network manager, messaging applications, cloud drive client, etc.

```
# --- Start up applications --- #
exec --no-startup-id nm-applet 
exec --no-startup-id diodon
exec --no-startup-id gnome-ring --restore-last-window-state
exec --no-startup-id /usr/bin/owncloud
exec --no-startup-id /opt/telegram/Telegram
```

With a tidy `config` file start up applications management can actually be easier than in Unity.

Assigning applications to work spaces at start up
-------------------------------------------------

When multiple applications are initiated at start up it is useful to spread them around in specific work places. Unfortunately, this is not possible in i3. There are several guides out there promising such functionality, but none actually works. At least not on Ubuntu.

This is still an active area of research, if a solution is ever found, it will be added to this space. 
