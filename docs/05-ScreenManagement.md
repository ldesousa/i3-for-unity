Screen output management
========================

When i3 starts it identifies the correct size and resolution of all the screens connected to the system. It creates an initial workspace on each of the screens and randomly spreads around the start up applications. After that the user is on its own and must manually apply the necessary modifications in case a new screen is connected to, or disconnected from, the system. This can be rather challenging with laptops, that tend to frequently connect to different screens or other video outputs.

Although other methods exist to manage video output, the easiest is by far to use the [`arandr`](https://christian.amsuess.com/tools/arandr/) tool, that provides an intuitive graphical interface. This programme is not installed by default on Ubuntu, but can be obtained from the official repositories:

```sudo apt install arandr```

On laptops it might be useful to create a shortcut to `arandr`. For instance, using the `a` key in the i3 `config` file:

`bindsym $mod+a exec arandr`

`arandr` is simple to use and immediately provides a visual understanding of the changes performed to video output. It is therefore not really necessary to explain its functioning here. However, for more on this programme, please visit its [web page](https://christian.amsuess.com/tools/arandr/).
