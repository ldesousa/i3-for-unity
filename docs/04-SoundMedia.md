Sound and media controls
========================

Controlling sound output and media players is one of the most challenging things in i3. None of the keyboard shortcuts or *ad hoc* keys function from the onset, they all must be set purposely in the `config` file. However, it is not possible to fully set all these controls like a Unity user would expect. The configurations proposed below should function in most systems, but it is possible they may fail with certain combinations of software and hardware.

Volume controls
---------------

Most commercial keyboards include *ad hoc* keys for volume control. Likewise, most laptops (even the smallest) provide such functionality with the <kbd>Fn</kbd> key. Therefore, what is necessary in i3 is to send the signals from these keys (or key combinations) to the Pulse Audio programme. As always, this is achieved with key bindings set in the `config` file, using special key identifiers with the prefix `XF86Audio`. The code segment below provides an example for the volume up, volume down and mute keys.

```
# --- Pulse Audio controls --- #
#increase sound volume
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% 
#decrease sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5% 
# mute sound
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle 
```

In this example the volume up/down controls are discretised into 5% percent steps. The user may well modify the size of these steps at will.

Various keyboards, particularly ergonomic ones, lack the *ad hoc* volume control keys. In such cases the `XF86Audio` identifiers can simply be replaced by regular key combinations. For instance:

```
bindsym $mod++ exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% 

```

Media playback controls
-----------------------

Unlike Unity, i3 does not translate signals from playback controls to actions. Playback buttons or keys have no effect in i3 by default. As usual, getting them to work requires the explicit mapping from those controls to executable actions.

Unfortunately, media controls do not function universally with i3. There are various solutions to be found on the internet, but none applies to all system set ups. The solution proposed below is functional in about two thirds of the systems tested. 

`playerctl` is a programme that provides access to media players from the command line in many systems. It is not packaged for Ubuntu, but a `.deb` file can be obtained from the Debian repositories (check [this answer at Ask.Ubuntu](https://askubuntu.com/a/1088268/177437) to obtain the latest version):

```
cd /tmp
wget http://ftp.nl.debian.org/debian/pool/main/p/playerctl/playerctl_2.0.1-1_amd64.deb
wget http://ftp.nl.debian.org/debian/pool/main/p/playerctl/libplayerctl2_2.0.1-1_amd64.deb
dpkg -i libplayerctl2_2.0.1-1_amd64.deb
dpkg -i playerctl_2.0.1-1_amd64.deb 
```

The final trick is to find the identifiers of the playback controls. That known, they can be mapped to actions with `playerctl`:

```
# Media player controls
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioPause exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous
```

Sound output
------------

In Unity, every time the user plugs in an alternative sound device, e.g. headphones, HDMI device, the DE automatically diverts the output to it. This is another convenience that is not present in i3. Once an alternative device is connected, sound output must the diverted manually.

This is a similar situation to [video output](05-ScreenManagement.md), being best managed from a user interface. The catch here is that the Gnome settings application shipped with Ubuntu does not function correctly on i3. However, it is possible to invoke the Gnome control centre programme with a specific environment setting. Since this can not be made directly from the application launcher, adding a specific binding in the `config` file is again useful:

```
# Bind System settings
bindsym $mod+z exec env XDG_CURRENT_DESKTOP=GNOME gnome-control-center 
```

As its name implies, the Gnome control centre can be used to manage many other aspects of the system. Handy in other tasks beyond sound output settings.

