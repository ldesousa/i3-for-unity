Lock, suspend and shutdown
==========================

Another striking difference between i3 and Unity, is the lack of any visible mechanisms to suspend or shutdown the computer. In normal circumstances, the user must quit the i3 session and only then use the controls available in the Ubuntu log on screen. This is somewhat inconvenient, especially if the user only wishes to lock the screen while away.  

Lock screen
-----------

i3 provides a specific tool to look the screen, named `i3look`. It should get installed together with the main DE package, but it is good  to verify whether it is installed:

```
sudo apt install i3lock
```

As usual, all it takes to activate `i3lock` is to add a new entry in the `config` file. In the example below it is mapped to the Ctrl+Mod+L key combination, similar to the Ctrl+Alt+L combination in Unity:

```
bindsym $mod+Control+l exec i3lock -i /home/lads/Wallpapers/ScreenSaver.png
```

As can be seen in the example above, the `-i` argument can be used to pass the path to a wallpaper displayed when the screen is locked. 


**Carefull**: in i3 the screen never locks when the monitor switches off due to inactivity; the `i3lock` programme must always be executed explicitly. When the monitor is switched off due to inactivity, the user must make sure it gets switched on before entering the password to unlock. Otherwise the password is written in clear text in whatever programme is open at the time.

Suspend the session
-------------------

There is no specific programme in i3 to suspend the computer, since the system itself provides ways to do so. In Ubuntu it can easily be done with `systemctl`, also simple to map in the `config` file:

```
bindsym $mod+Control+s exec --no-startup-id systemctl suspend
```

Shutdown
--------

Most linux distributions ship with a command line tool called `shutdown` that exits the session and turns off the computer in an orderly way. It takes at least one argument, the time in seconds until the shutdown sequence is started.

It is easy to create a new key binding in the i3 `config` file with `shutdown` for a quick and easy switch off. However, this method is not recommended. No confirmation is required, once the `shutdown` command is issued there is no turning back and any unsaved work will be lost. Accidentally pressing the key binding to siwtch off can be a small tragedy.

Instead it is preferable to initiate the shutdown in an explicit way. First opening a new terminal and then issuing the command:

`$ shutdown 0`

The small sequence of actions required in this way provide time for the user to think whether turning off the computer is appropriate at the time or not.

