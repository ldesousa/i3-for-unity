More
====

Found something wrong? Is there a more efficient way to achieve a certain set up bit? Is an important aspect missing? Please contribute to this manual by opening issues or creating pull requests at the main repository at [Codeberg](https://codeberg.org/ldesousa/i3-for-unity).

This manual can still grow considerably, further aspects of i3 will be approached in future releases. Contributions are very welcome!
