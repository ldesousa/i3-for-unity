Connecting to the internet
==========================

One of the first noticeable things for a first time user of i3 is the absence of internet connection. With a vanilla install, an i3 session does not connect to any known WiFi networks, nor does it provide the functionality to do so.

This is possibly the first issue an i3 user must tackle after install. Luckily, it is relatively easy to solve on Ubuntu, it simply requires the installation and set up of a networking manager programme. In the following example the `nm-applet` programme is used, but any similar programme can be used.

`nm-applet` is provided by the package `network-manager-gnome`, that can be installed with:

```
sudo apt install network-manager-gnome
```

The programme can be run from the command line, but can also be started directly from the i3 `config` file with the following directive:


```
exec --no-startup-id nm-applet #Network Manager
```

The `nm-applet` programme is required every time an i3 session is created, therefore it should be in the section of Start up applications in the i3 `config` file. More on that aspect of i3 in the [Start up applications](03-StartUp.md) section.
