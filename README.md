i3 for Unity users
==================

A manual detailing how to set up the [i3](https://i3wm.org/) desktop environment on Ubuntu. Concieved for users of the now defunct [Unity](https://en.wikipedia.org/wiki/Unity_%28user_interface%29) desktop environment.

[![Documentation Status](https://readthedocs.org/projects/i3-for-unity-users/badge/?version=latest)](https://i3-for-unity-users.readthedocs.io/en/latest/?badge=latest)

Copyright
--------------------------------------------------------------------------------

Copyright 2019 Luís Moreira de Sousa. All rights reserved.
Any use of these documents constitutes full acceptance of all terms of the
documents licence.

Licence
-------

This set of scripts is released under the European Union Public Licence v 1.2. Please consult the LICENCE file for details.
